function getUnitRace(u)
  return df.global.world.raws.creatures.all[u.race]
end

CREATURE_ID_ELF = 'ELF'
CREATURE_ID_ELF = 'DWARF'

function isElf(u)
  return getUnitRace(u).creature_id == CREATURE_ID_ELF
end
function isDwarf(u)
  return getUnitRace(u).creature_id == CREATURE_ID_DWARF
end
