WORKSHOP_MAX_JOBS = 10

local utils=require 'utils'
local gscript=require 'gui.script'

gscript.start(function()
  slabcount = 0
  decorated = 0
  undecorated = 0
  engraved = {}
  -- Find undecorated slabs
  for idx,slab in ipairs(df.global.world.items.other.SLAB) do
    slabcount = slabcount + 1
    if slab.engraving_type == df.slab_engraving_type.Memorial then
      engraved[slab.topic] = true
      decorated = decorated + 1
    else
      undecorated = undecorated + 1
    end
  end

  -- Find spooky ghosts
  ghostcount = 0
  ghosts = {}
  for idx,u in ipairs(df.global.world.units.all) do
    if u.race == df.global.ui.race_id and u.flags3.ghostly then
      if engraved[u.id] == nil then
        table.insert(ghosts, u)
      end
    end
  end

  -- Iterate over ghosts
  for _, ghost in ipairs(ghosts) do
    order_issued = false
    for link,job in utils.listpairs(df.global.world.jobs.list) do
      local job = link.item
      local place = dfhack.job.getHolder(job)

      if job.job_type == df.job_type.EngraveSlab
      and job.hist_figure_id == ghost.hist_figure_id then
        order_issued = true
        break
      end
    end
    local available_building = nil
    for _, bld in ipairs(df.global.world.buildings.other[df.buildings_other_id.WORKSHOP_CRAFTSDWARF]) do
      if #bld.jobs < WORKSHOP_MAX_JOBS then
        available_building = bld
        break
      end
    end
    if not order_issued and available_building ~= nil then
      --[[local o=df.manager_order:new()
      o.id=df.global.world.manager_order_next_id
      df.global.world.manager_order_next_id=df.global.world.manager_order_next_id+1

      o.job_type = df.job_type.EngraveSlab
      o.hist_figure_id = ghost.hist_figure_id
      o.amount_left = 1
      o.amount_total = 1
      df.global.world.manager_orders:insert('#', o)
      print("Created job #"..o.id.."!")
      ]]--
      local newJob=df.job:new()
      newJob.id=df.global.job_next_id
      df.global.job_next_id=df.global.job_next_id+1
      --newJob.flags.special=true
      newJob.job_type=df.job_type.EngraveSlab
      newJob.completion_timer=-1
      newJob.hist_figure_id=ghost.hist_figure_id

      newJob.pos.x = available_building.centerx
      newJob.pos.y = available_building.centery
      newJob.pos.z = available_building.z
      newJob.general_refs:insert("#",{new=df.general_ref_building_holderst, building_id=available_building.id})
      available_building.jobs:insert("#",newJob)
      --newJob.pos:assign(args.unit.pos)
      dfhack.job.linkIntoWorld(newJob)
      print("Created job #"..newJob.id.."!")
      --args.screen:wait_tick()
    end
  end
end)
