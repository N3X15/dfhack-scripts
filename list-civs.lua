-- Dismisses stuck merchants that haven't entered the map yet
-- Based on "dismissmerchants" by PatrikLundell:
-- http://www.bay12forums.com/smf/index.php?topic=159297.msg7257447#msg7257447

help = [====[

fix/stuck-merchants
===================

Dismisses merchants that haven't entered the map yet. This can fix :bug:`9593`.
This script should probably not be run if any merchants are on the map, so using
it with `repeat` is not recommended.

Run ``fix/stuck-merchants -n`` or ``fix/stuck-merchants --dry-run`` to list all
merchants that would be dismissed but make no changes.

]====]


function getEntityName(u)
    local civ = df.historical_entity.find(u.civ_id)
    if not civ then return 'unknown civ' end
    return dfhack.TranslateName(civ.name)
end

function getEntityRace(u)
    local civ = df.historical_entity.find(u.civ_id)
    if civ then
        local craw = df.creature_raw.find(civ.race)
        if craw then
            return craw.name[0]
        end
    end
    return 'unknown race'
end

function dismissMerchants(args)
    --for _,u in pairs(df.global.world.units.active) do
	for civ_id, civ in pairs(df.historical_entity) do
		print(civ_id, civ.name, dfhack.df2console(dfhack.TranslateName(civ.name)), dfhack.df2console(df))
    end
end

if not dfhack_flags.module then
    dismissMerchants{...}
end
