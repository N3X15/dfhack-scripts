local function ensureValIs(base, subject, subj_long, required)
	value = base[subject]
	if value ~= required then
		dfhack.println(subj_long..": "..tostring(value).." -> "..tostring(required))
		base[subject] = required
	end
end

dorf = dfhack.gui.getSelectedUnit()
ensureValIs(dorf,        "mood_copy",    "Mood (copy)",  df.mood_type.None)
ensureValIs(dorf,        "mood",         "Mood",         df.mood_type.None)
ensureValIs(dorf,        "profession2",  "Profession 2", df.profession.STANDARD)
ensureValIs(dorf.flags1, "had_mood",     "Had Mood",     false)
ensureValIs(dorf.flags1, "has_mood",     "Has Mood",     false)
ensureValIs(dorf.job,    "mood_skill",   "Mood Skill",   0)
ensureValIs(dorf.job,    "mood_timeout", "Mood Timeout", 0)
ensureValIs(dorf,        "mount_type",   "Mount Type",   0)
ensureValIs(dorf.job,    "unk_v40_2",    "Unknown v40 #2", 0)
ensureValIs(dorf.job,    "unk_v40_4",    "Unknown v40 #4", 0)

-- Workshop fixes
if dorf.job ~= nil and dorf.job.current_job ~= nil then
	local job = dorf.job.current_job
	ensureValIs(job,       "job_type", "Job Type",         df.job_type.NONE)
	ensureValIs(job.flags, "special",  "Job is Special",   false)
	ensureValIs(job.flags, "suspend",  "Job is Suspended", true)
	dfhack.println("You should now be able to remove the job from the workshop!")
end
