TWEAKABLE_CHANCE_OF_REMOVAL=5 -- percent chance that the mud/blood/etc is removed this tick.
TWEAKABLE_NUM_CYCLES_PER_PERIOD=1
TWEAKABLE_PERIOD_TYPE='days'


local function _input_boolean(v)
  if v == "1" then
    return true
  elseif v == "y" then
    return true
  elseif v == "yes" then
    return true
  elseif v == "true" then
    return true
  elseif v == "0" then
    return false
  elseif v == "n" then
    return false
  elseif v == "no" then
    return false
  elseif v == "false" then
    return false
  else
    dfhack.println("[decay-contaminants] Invalid boolean. Use 0 or 1.")
    return false
  end
end

local function _is_mat_cleanable(bev)
  if snow and bev.mat_type == df.builtin_mats.WATER and bev.mat_state == df.matter_state.POWDER then
    return true
  end
  if mud and bev.mat_type == df.builtin_mats.MUD and bev.mat_state == df.matter_state.SOLID then
    return true
  end
  if mud and bev.mat_type == df.builtin_mats.VOMIT then
    return true
  end
end

if args[1] == "enable" then
    enabled = true
elseif args[1] == "disable" then
    enabled = false
elseif args[1] == "set" then
  if args[2] == "mud" then
    mud = _input_boolean(args[3])
  elseif args[2] == "snow" then
    snow = _input_boolean(args[3])
  elseif args[2] == "vomit" then
    vomit = _input_boolean(args[3])
  elseif args[2] == "blood" then
    blood = _input_boolean(args[3])
  else
    dfhack.println("[decay-contaminants] Valid <set> variables:")
    dfhack.println(" - blood")
    dfhack.println(" - mud")
    dfhack.println(" - snow")
    dfhack.println(" - vomit")
  end
end

-- https://gist.github.com/Uradamus/10323382
function shuffle(tbl)
  size = #tbl
  for i = size, 1, -1 do
    local rand = math.random(size)
    tbl[i], tbl[rand] = tbl[rand], tbl[i]
  end
  return tbl
end

local function getBlock(x,y,z)
  return df.global.world.map.block_index[x][y][z]
end

local function isContaminated(block_events)
  for i=0,#block_events,1 do
    local bev = block_events[i]
    if bev:getType() == df.block_square_event_type.material_spatter then
      return true
    end
  end
  return false
end

local function check_contaminants()
  local contaminated = {}
  local contam_map = {}
  local sz_x, sz_y, sz_z = dfhack.maps.getSize()
  for z = 0,sz_z,1 do
    for y = 0,sz_y,1 do
      for x = 0,sz_x,1 do
        local block_events = getBlock(x,y,z).block_events
        for i=0,#block_events,1 do
          local bev = block_events[i]
          if bev:getType() == df.block_square_event_type.material_spatter and _is_mat_cleanable(bev) then
            -- Local offsets. *screaming intensifies*
            --for lz=0,16,1 do
              for ly=0,16,1 do
                for lx=0,16,1 do
                  gx = (x*16)+lx
                  gy = (y*16)+ly
                  key = {'x'=gx,'y'=gy,'z'=z} --tostring(gx)..","..tostring(gy)..","..tostring(z)
                  if bev.amount[lx][ly] > 0 and contaminated[key] ~= nil then
                    table.insert(contaminated, key)
                    contam_map[key] = true
                  end
                end
              end
            --end
          end
        end
      end
    end
  end
  if #contaminated > 0 then
    dfhack.println("[decay-contaminants] Found %d contaminated tiles.":format(#contaminated))
    shuffle(contaminated)
    for i=0,#contaminated,1 do

    end
  end
end

local function event_loop()
    if enabled then
        check_contaminants()
        dfhack.timeout(TWEAKABLE_NUM_CYCLES_PER_PERIOD, TWEAKABLE_PERIOD_TYPE, event_loop)
    end
end

dfhack.onStateChange.loadDecayContaminants = function(code)
    if code==SC_MAP_LOADED then
        if enabled then
            print("decay-contaminants enabled.")
            event_loop()
        else
            print("decay-contaminants disabled.")
        end
    end
end

if dfhack.isMapLoaded() then
    dfhack.onStateChange.loadDecayContaminants(SC_MAP_LOADED)
end
