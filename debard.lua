-- Fuck bards ok

-- Hard cap on all visitors.
MAX_VISITORS = 20


MODE_POOF = 0 -- They vanish in an annoyingly-long-lasting puff of smoke, leaving clothing and lag in their wake.
MODE_HYPERTHERMIA = 1 -- UNTESTED: Induces hypothermia
MODE_HYPOTHERMIA = 2 -- UNTESTED: Induces hyperthermia
MODE_POISON = 3 -- UNTESTED: Gives them a shot of serpent man venom.
MODE_LEAVE = 4 -- They just walk off.
MODE_SLAUGHTER = 5 -- UNTESTED: BLOOD FOR THE BLOOD GOD

MODE = MODE_LEAVE

-- Cap per occupation
MAX_OCCUPATIONS = {} -- NO TOUCH
MAX_OCCUPATIONS[df.profession.BARD] = 5
MAX_OCCUPATIONS[df.profession.SCHOLAR] = 10
MAX_OCCUPATIONS[df.profession.SWORDSMAN] = 5 -- Mercs

--[[
END OF USER CONFIGURATION
]]--

local syndromeUtil = require 'syndrome-util'

-- Probably missing some.
ALIASES = {}
ALIASES[df.profession.PERFORMER] = df.profession.BARD
ALIASES[df.profession.POET] = df.profession.BARD
--ALIASES[df.profession.BARD] = df.profession.BARD
ALIASES[df.profession.DANCER] = df.profession.BARD

ALIASES[df.profession.SAGE] = df.profession.SCHOLAR
--ALIASES[df.profession.SCHOLAR] = df.profession.SCHOLAR
ALIASES[df.profession.PHILOSOPHER] = df.profession.SCHOLAR
ALIASES[df.profession.MATHEMATICIAN] = df.profession.SCHOLAR
ALIASES[df.profession.HISTORIAN] = df.profession.SCHOLAR
ALIASES[df.profession.ASTRONOMER] = df.profession.SCHOLAR
ALIASES[df.profession.NATURALIST] = df.profession.SCHOLAR
ALIASES[df.profession.CHEMIST] = df.profession.SCHOLAR
ALIASES[df.profession.GEOGRAPHER] = df.profession.SCHOLAR
ALIASES[df.profession.SCRIBE] = df.profession.SCHOLAR
ALIASES[df.profession.ENGINEER] = df.profession.SCHOLAR

ALIASES[df.profession.HAMMERMAN] = df.profession.SWORDSMAN
ALIASES[df.profession.MASTER_HAMMERMAN] = df.profession.SWORDSMAN
ALIASES[df.profession.SPEARMAN] = df.profession.SWORDSMAN
ALIASES[df.profession.MASTER_SPEARMAN] = df.profession.SWORDSMAN
ALIASES[df.profession.CROSSBOWMAN] = df.profession.SWORDSMAN
ALIASES[df.profession.MASTER_CROSSBOWMAN] = df.profession.SWORDSMAN
ALIASES[df.profession.WRESTLER] = df.profession.SWORDSMAN
ALIASES[df.profession.MASTER_WRESTLER] = df.profession.SWORDSMAN
ALIASES[df.profession.AXEMAN] = df.profession.SWORDSMAN
ALIASES[df.profession.MASTER_AXEMAN] = df.profession.SWORDSMAN
--ALIASES[df.profession.SWORDSMAN] = df.profession.SWORDSMAN
ALIASES[df.profession.MASTER_SWORDSMAN] = df.profession.SWORDSMAN
ALIASES[df.profession.MACEMAN] = df.profession.SWORDSMAN
ALIASES[df.profession.MASTER_MACEMAN] = df.profession.SWORDSMAN
ALIASES[df.profession.PIKEMAN] = df.profession.SWORDSMAN
ALIASES[df.profession.MASTER_PIKEMAN] = df.profession.SWORDSMAN
ALIASES[df.profession.BOWMAN] = df.profession.SWORDSMAN
ALIASES[df.profession.MASTER_BOWMAN] = df.profession.SWORDSMAN
ALIASES[df.profession.BLOWGUNMAN] = df.profession.SWORDSMAN
ALIASES[df.profession.MASTER_BLOWGUNMAN] = df.profession.SWORDSMAN
ALIASES[df.profession.BLOWGUNMAN] = df.profession.SWORDSMAN
ALIASES[df.profession.MASTER_BLOWGUNMAN] = df.profession.SWORDSMAN

-- Stolen and modified from emigration
function canLeave(unit)
    for _, skill in pairs(unit.status.current_soul.skills) do
      if skill.rating > 14 then return false end
    end
    if unit.flags1.caged
      or dfhack.units.isDead(unit)
      or dfhack.units.isOpposedToLife(unit)
      or unit.flags1.merchant
      or unit.flags1.diplomat
      or unit.flags1.chained
      or dfhack.units.getNoblePositions(unit) ~= nil
      or unit.military.squad_id ~= -1
      or dfhack.units.isCitizen(unit)
      or not dfhack.units.isSane(unit)
    then
      return false
    end
    return true
end

nOccupations={}
nOccupations[df.profession.BARD] = 0
nOccupations[df.profession.SCHOLAR] = 0
nOccupations[df.profession.SWORDSMAN] = 0 -- Mercs

nVisitors = 0
for idx, dorf in ipairs(df.global.world.units.all) do
  if dorf.flags2.visitor then
    pkey = dorf.profession
    if ALIASES[dorf.profession] ~= nil then
      pkey = ALIASES[dorf.profession]
    end
    if nOccupations[pkey] == nil then
      print("Warning: Unidentified profession "..dfhack.units.getProfessionName(dorf).." ("..tostring(dorf.profession)..")")
      nOccupations[pkey] = 0
    end
    nOccupations[pkey] = nOccupations[pkey] + 1
    nVisitors = nVisitors + 1

    if canLeave(dorf) and (math.random(100) < 5 or nVisitors > MAX_VISITORS or (MAX_OCCUPATIONS[pkey] ~= nil and nOccupations[pkey] > MAX_OCCUPATIONS[pkey])) then
      -- End any current jobs.
      if dorf.job ~= nil and dorf.job.current_job ~= nil then
        local job = dorf.job.current_job
      	job.job_type = df.job_type.NONE
      	job.flags.special = false
      	job.flags.suspend = true
      end

      if MODE == MODE_POOF then
        -- This is hilarious and laggy
        dorf.animal.vanish_countdown = 1
      elseif MODE_LEAVE == MODE then
        -- Tell the unit to FUCK OFF.
        dorf.idle_area_threshold = 0
        dorf.idle_area_type = df.unit_station_type.HeadForEdge
        dorf.civ_id = -1
        dorf.following = nil
        dorf.flags1.forest = true
        dorf.animal.leave_countdown = 2
        dorf.flags2.visitor = true
      elseif MODE == MODE_HYPERTHERMIA then
        for i, temp in ipairs(unit.status2.body_part_temperature) do
          local bp = unit.body.body_plan.body_parts[i]
          temp.whole = bp.max_temp * 2
        end
      elseif MODE == MODE_HYPOTHERMIA then
        for i, temp in ipairs(unit.status2.body_part_temperature) do
          local bp = unit.body.body_plan.body_parts[i]
          temp.whole = 0
        end
      elseif MODE == MODE_POISON then
        syndromeUtil.infectWithSyndrome(dorf, 'serpent man venom', 'DoNothing')
      elseif MODE == MODE_SLAUGHTER then
        dorf.flags2.slaughter = true
      end

      local line = dfhack.TranslateName(dfhack.units.getVisibleName(dorf)) .. " has left the fortress."
      print(line)
      dfhack.gui.showAnnouncement(line, COLOR_GREEN)
    end
  end
end
