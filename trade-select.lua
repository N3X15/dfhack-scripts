local help = [===[
Accepts:
  -allow-mandated
  -allow-container
  -allow-wood
]===]
local utils = require 'utils'

local args = {...}

tradeview = df.global.gview.view.child.child
tradegoods = tradeview.child

trader_stuff = {}
broker_stuff = {}

function table.contains(table, element)
  for _, value in pairs(table) do
    if value == element then
      return true
    end
  end
  return false
end

for i, item in ipairs(tradegoods.trader_items) do
  e = {
    idx = i,
    item = item,
    selected = (tradegoods.trader_selected[i]==1),
    count = tradegoods.trader_count[i]
  }
  table.insert(trader_stuff, e)
end

for i, item in ipairs(tradegoods.broker_items) do
  e = {
    idx = i,
    item = item,
    selected = (tradegoods.broker_selected[i]==1),
    count = tradegoods.broker_count[i]
  }
  table.insert(broker_stuff, e)
end

function trader_select(entry, value)
  tradegoods.trader_selected[entry.idx]=(value and 1 or 0)
end

function broker_select(entry, value)
  tradegoods.broker_selected[entry.idx]=(value and 1 or 0)
end

function _inner_BannedFromExport(entry, mandate)
  if mandate.mode ~= 0 then
    return false end
  if entry.item:getType() ~= mandate.item_type or (mandate.item_subtype ~= -1 and entry.item:getSubtype() ~= mandate.item_subtype) then
    return false end
  if mandate.mat_type ~= -1 and entry.item.mat_type ~= mandate.mat_type then
    return false end
  if mandate.mat_index ~= -1 and entry.item.mat_index ~= mandate.mat_index then
    return false end
  return true
end

function IsBannedFromExport(entry)
  -- See check_mandates() in autotrade.cpp.
  for _,mandate in ipairs(df.global.world.mandates) do
    if _inner_BannedFromExport(entry, mandate) then
      return true
    end
  end
  return false
end

local total_weight = 0
local containers={}
for i, entry in ipairs(broker_stuff) do
  use = (function()
    if not table.contains(args,'-allow-mandated') then
      if IsBannedFromExport(entry) then
        dfhack.print("Skipping item #"..tostring(entry.item.id).." due to mandate.\n")
        return false -- continue
      end
    end

    if not table.contains(args,'-allow-container') then
      if entry.item.flags.container then
        dfhack.print("Skipping item #"..tostring(entry.item.id).." because it's a container.\n")
        return false -- continue
      end
    end

    if not table.contains(args,'-allow-wood') then
      matinfo = dfhack.matinfo.decode(entry.item)
      if matinfo.material.flags.WOOD then
        dfhack.print("Skipping item #"..tostring(entry.item.id).." because it's wooden.\n")
        return false -- continue
      end
    end

    return true
  end)()
  if (total_weight + entry.item.weight) >= tradegoods.caravan.total_capacity then
    dfhack.print("Skipping item #"..tostring(entry.item.id).." because the caravan would be overweight.\n")
    use = false
  end
  if use then
    total_weight = total_weight + entry.item.weight
  end
  broker_select(entry, use)
end
