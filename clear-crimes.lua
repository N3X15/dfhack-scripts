local function ensureValIs(base, subject, subj_long, required)
	value = base[subject]
	if value ~= required then
		dfhack.println(subj_long..": "..tostring(value).." -> "..tostring(required))
		base[subject] = required
	end
end

local cid = 0
local cleared = 0
local innocent = {}
while cid < #df.global.world.crimes.all do
  local crime = df.global.world.crimes.all[cid]
  if crime.mode == df.crime.T_mode.ProductionOrderViolation -- 0
  or crime.mode == df.crime.T_mode.ExportViolation          -- 1
  or crime.mode == df.crime.T_mode.JobOrderViolation        -- 2
  or crime.mode == df.crime.T_mode.ConspiracyToSlowLabor    -- 3
  then
    if crime.punishment.hammerstrikes > 0
    or crime.punishment.give_beating > 0
    or crime.punishment.prison_time > 1
    then
      crime.punishment.hammerstrikes = 0
      crime.punishment.prison_time   = 1
      crime.punishment.give_beating  = 0
      --df.global.world.crimes.all:erase(cid)
      cleared = cleared + 1
    end
    if innocent[crime.criminal] == nil then
      innocent[crime.criminal] = true
    end
  else
    if innocent[crime.criminal] ~= nil then
      innocent[crime.criminal] = false
    end
  end
  cid = cid + 1
end
dfhack.println("Cleared "..tostring(cleared).." dumb crimes (.all)")


cleared = 0
cid=0
while cid < #df.global.world.crimes.bad do
  local crime = df.global.world.crimes.bad[cid]
  if crime.mode == df.crime.T_mode.ProductionOrderViolation -- 0
  or crime.mode == df.crime.T_mode.ExportViolation          -- 1
  or crime.mode == df.crime.T_mode.JobOrderViolation        -- 2
  or crime.mode == df.crime.T_mode.ConspiracyToSlowLabor    -- 3
  then
    if crime.punishment.hammerstrikes > 0
    or crime.punishment.give_beating > 0
    or crime.punishment.prison_time > 1
    then
      crime.punishment.hammerstrikes = 0
      crime.punishment.prison_time   = 1
      crime.punishment.give_beating  = 0
      --df.global.world.crimes.all:erase(cid)
      cleared = cleared + 1
    end
    if innocent[crime.criminal] == nil then
      innocent[crime.criminal] = true
    end
  else
    if innocent[crime.criminal] ~= nil then
      innocent[crime.criminal] = false
    end
  end
  cid = cid + 1
end
dfhack.println("Cleared "..tostring(cleared).." dumb crimes (.bad)")

cid=0
cleared=0
while cid < #df.global.ui.punishments do
  local punishment = df.global.ui.punishments[cid]
  local opinion = innocent[punishment.criminal.id]
  if opinion == true then
    local dorf = punishment.criminal
    print("Freeing "..tostring(dfhack.units.getVisibleName(dorf)))
    dfhack.println("  Punishment:")
    ensureValIs(punishment, "beating",        "    * Beating", 0)
    ensureValIs(punishment, "hammer_strikes", "    * Hammer Strikes", 0)
    ensureValIs(punishment, "prison_counter", "    * Hard Time", 1)
  end
	cid=cid+1
end
