function getUnitRace(u)
  return df.global.world.raws.creatures.all[u.race]
end

CREATURE_ID_ELF = 'ELF'
CREATURE_ID_DWARF = 'DWARF'

function isElf(u)
  return getUnitRace(u).creature_id == CREATURE_ID_ELF
end
function isDwarf(u)
  return getUnitRace(u).creature_id == CREATURE_ID_DWARF
end

teleport = reqscript 'teleport'

local pos = copyall(df.global.cursor)
if pos.x == -30000 then
    qerror("Cursor must be pointing somewhere")
end

for _, unit in ipairs(df.global.world.units.all) do
  if unit.pos.z <= 122 and isDwarf(unit) then
    unit_name=dfhack.TranslateName(dfhack.units.getVisibleName(unit))
    posstr=tostring(unit.pos.x)..","..tostring(unit.pos.y)..","..tostring(unit.pos.z)
    suffix = ""
    if unit.flags1.dead then
      suffix = suffix .. " (dead)"
    end
    dfhack.println(unit_name.." is stuck at "..posstr.."."..suffix)
    teleport.teleport(unit, pos)
  end
end
