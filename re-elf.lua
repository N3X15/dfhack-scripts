-- This will hopefully unfuck elf diplomacy.

local help = [====[

hax/re-elf [-h] [-n]
===================

 -n: Dry run
 
Hopefully fixes elf diplomacy bugs.

]====]

-- civ_id: 267
-- population_id: 244

-- DORF
-- civ: 299
-- pop: 260

-- //historical_entity.T_unknown1b.diplomacy
function getEntityName(civ_id)
    local civ = df.historical_entity.find(civ_id)
    if not civ then return 'unknown civ' end
    return dfhack.TranslateName(civ.name)
end

function getEntityRace(civ_id)
    local civ = df.historical_entity.find(civ_id)
    if civ then
        local craw = df.creature_raw.find(civ.race)
        if craw then
            return craw.name[0]
        end
    end
    return 'unknown race'
end

function getDiplomacyFromCiv(civ)
  for did, diplomacy in ipairs(civ.unknown1b.diplomacy) do
    print(('  DP %d - vs %d (%s, %s), rel=%d, a1=%d, a2=%d'):format(
      did,
      diplomacy.group_id,
      dipdfhack.df2console(getEntityName(diplomacy.group_id)),
      getEntityRace(diplomacy.group_id)
    ))
  end
end

function dismissMerchants(args)
    local dry_run = false
    for _, arg in pairs(args) do
        if args[1]:match('-h') or args[1]:match('help') then
            print(help)
            return
        elseif args[1]:match('-n') or args[1]:match('dry') then
            dry_run = true
        end
    end
    local removed = 0
    for _,u in pairs(df.global.world.units.active) do
        if dfhack.units.isMerchant(u) and dfhack.units.isDead(u) then
            --[[print(('%s unit %d: %s (%s), civ %d (%s, %s)'):format(
                dry_run and 'Would remove' or 'Removing',
                u.id,
                dfhack.df2console(dfhack.TranslateName(dfhack.units.getVisibleName(u))),
                df.creature_raw.find(u.race).name[0], 
                u.civ_id,
                dfhack.df2console(getEntityName(u)),
                getEntityRace(u)
            ))]]
            removed = removed + 1
            if not dry_run then
                u.flags1.left = true
            end
        end
    end
    local fmt = dry_run and 'Would remove %d units.' or 'Removed %d units.'
	print(fmt:format(removed))
end

if not dfhack_flags.module then
    dismissMerchants{...}
end
